from django.urls import path
from user.views import register


app_name = "users"

urlpatterns = [
    path("register/", register, name="register"),
]
