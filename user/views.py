from django.contrib.auth import login
from django.shortcuts import render, redirect
from django.urls import reverse

from user.forms import CreateUserForm


# Create your views here.
def register(request):
    form = CreateUserForm()
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect(reverse("rent_app:index"))
    return render(request, "user/register.html", {"form": form})
