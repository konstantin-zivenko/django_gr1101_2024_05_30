from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Count
from django.http import HttpResponse, HttpRequest, Http404, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views import generic

from . import db_services
from .forms import TagForm, ThingSearchForm
from .models import Thing, Category, Tag


def test_session_view(request: HttpRequest) -> HttpResponse:
    request.session["test"] = "test session value"
    return HttpResponse(
        "<h1>Session test</h1>"
        "<hr>"
        f"Session data: {request.session.items()}<br>"
        f"Session value: {request.session.get('test', 'No session value')}"
    )


def index(request: HttpRequest) -> HttpResponse:
    num_things = Thing.objects.count()
    num_categories = Category.objects.count()
    num_tags = Tag.objects.count()
    num_visits = request.session.get("num_visits", 0)
    request.session["num_visits"] = num_visits + 1
    context = {
        "num_things": num_things,
        "num_categories": num_categories,
        "num_tags": num_tags,
        "num_visits": num_visits,
    }
    return render(request, "rent_things/index.html", context=context)


class ThingListView(generic.ListView):
    model = Thing
    template_name = (
        "rent_things/things_list.html"  # Default: <app_label>/<model_name>_list.html
    )
    context_object_name = "things"  # Default: object_list
    queryset = Thing.objects.filter(is_active=True).select_related(
        "category"
    )  # Default: Model.objects.all()
    paginate_by = 10

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context["search_form"] = ThingSearchForm(initial=self.request.GET)
        return context

    def get_queryset(self):
        queryset = super().get_queryset()
        name = self.request.GET.get("name")
        categories = self.request.GET.getlist("categories")
        tags = self.request.GET.getlist("tags")
        if name:
            queryset = queryset.filter(name__icontains=name)
        if tags:
            queryset = queryset.filter(tags__in=tags).distinct()
        if categories:
            all_categories = db_services.categories.get_all_child_categories(categories)
            queryset = queryset.filter(category__in=all_categories).distinct()
        return queryset


class ThingDetailView(generic.DetailView):
    model = Thing
    template_name = (
        "rent_things/thing_detail.html"  # Default: <app_label>/<model_name>_detail.html
    )
    context_object_name = "thing"  # Default: object
    queryset = Thing.objects.filter(is_active=True)  # Default: Model.objects.all()


class CategoryListView(generic.ListView):
    model = Category
    context_object_name = "categories"
    queryset = Category.objects.filter(is_active=True).annotate(
        Count("things")
    )  # Default: Model.objects.all()
    paginate_by = 5


class CategoryDetailView(generic.DetailView):
    model = Category
    context_object_name = "category"
    queryset = (
        Category.objects.filter(is_active=True)
        .select_related("parent")
        .prefetch_related("children")
    )


class CategoryCreateView(LoginRequiredMixin, generic.CreateView):
    model = Category
    fields = ["name", "description", "parent"]
    template_name = "rent_things/category_form.html"

    def get_success_url(self):
        return reverse_lazy("rent_app:category_detail", kwargs={"pk": self.object.pk})


class CategoryUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = Category
    fields = ["name", "description", "parent"]
    template_name = "rent_things/category_form.html"

    def get_success_url(self):
        return reverse_lazy("rent_app:category_detail", kwargs={"pk": self.object.pk})


class CategoryDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Category
    template_name = "rent_things/category_confirm_delete.html"
    success_url = reverse_lazy("rent_app:category_list")

    def get_queryset(self):
        return Category.objects.filter(is_active=True).annotate(
            things_count=Count("things")
        )


class TagListView(generic.ListView):
    model = Tag
    context_object_name = "tags"
    queryset = (
        Tag.objects.filter(is_active=True)
        .annotate(things_count=Count("things"))
        .values("id", "name", "things_count")
    )


class TagDetailView(generic.DetailView):
    model = Tag
    context_object_name = "tag"
    queryset = Tag.objects.filter(is_active=True)


class TagCreateView(LoginRequiredMixin, generic.CreateView):
    model = Tag
    form_class = TagForm
    template_name = "rent_things/tag_form.html"
    success_url = reverse_lazy("rent_app:tag_list")


class TagUpdateView(LoginRequiredMixin, generic.UpdateView):
    model = Tag
    form_class = TagForm
    template_name = "rent_things/tag_form.html"
    success_url = reverse_lazy("rent_app:tag_list")


class TagDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Tag
    template_name = "rent_things/tag_confirm_delete.html"
    success_url = reverse_lazy("rent_app:tag_list")

    def get_queryset(self):
        return Tag.objects.filter(is_active=True).annotate(things_count=Count("things"))
