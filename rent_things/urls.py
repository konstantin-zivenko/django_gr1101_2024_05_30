from django.urls import path
from rent_things.views import (
    test_session_view,
    index,
    ThingListView,
    CategoryListView,
    CategoryDetailView,
    CategoryCreateView,
    CategoryUpdateView,
    CategoryDeleteView,
    ThingDetailView,
    TagDetailView,
    TagListView,
    TagCreateView,
    TagDeleteView,
    TagUpdateView,
)


app_name = "rent_app"

urlpatterns = [
    path("test-session/", test_session_view, name="test_session"),
    path("", index, name="index"),
    path("things/", ThingListView.as_view(), name="things_list"),
    path("things/<int:pk>/", ThingDetailView.as_view(), name="thing_detail"),
    path("categories/", CategoryListView.as_view(), name="category_list"),
    path("categories/<int:pk>/", CategoryDetailView.as_view(), name="category_detail"),
    path("categories/create/", CategoryCreateView.as_view(), name="category_create"),
    path(
        "categories/<int:pk>/delete/",
        CategoryDeleteView.as_view(),
        name="category_delete",
    ),
    path(
        "categories/<int:pk>/update/",
        CategoryUpdateView.as_view(),
        name="category_update",
    ),
    path("tags/", TagListView.as_view(), name="tag_list"),
    path("tags/<int:pk>/", TagDetailView.as_view(), name="tag_detail"),
    path("tags/create/", TagCreateView.as_view(), name="tag_create"),
    path("tags/<int:pk>/delete/", TagDeleteView.as_view(), name="tag_delete"),
    path("tags/<int:pk>/update/", TagUpdateView.as_view(), name="tag_update"),
]
