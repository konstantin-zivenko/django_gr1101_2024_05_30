import warnings

from django.db.models import QuerySet

from rent_things import models


def retrieve(
    id_: int = None,
    /,
    name: str = None,
    description: str = None,
    is_active: bool = True,
) -> QuerySet:
    if id_ and (name or description or is_active):
        warnings.warn(
            f"{retrieve.__name__} function when passing argument to id_, other parameters are ignored"
        )
        return models.Tag.objects.get(pk=id_)
    if id_:
        return models.Tag.objects.filter(id=id_)
    queryset = models.Tag.objects.filter(is_active=is_active)
    if name:
        queryset = queryset.filter(name=name)
    if description:
        queryset = queryset.filter(description=description)
    return queryset


def create(name: str, description: str = None) -> models.Tag:
    return models.Tag.objects.create(name=name, description=description)
