from django.db.models import QuerySet

from rent_things.models import Category


def get_all_child_categories(category_ids: list[str] = None) -> QuerySet:
    if category_ids:
        return Category.objects.raw(
            f"""
            WITH RECURSIVE cte(id, parent_id) AS (
                SELECT id, parent_id
                FROM rent_things_category
                WHERE id IN ({', '.join(map(lambda x: x, category_ids))}) AND is_active = TRUE
            UNION ALL
                SELECT rent_things_category.id, rent_things_category.parent_id
                FROM rent_things_category
                JOIN cte ON rent_things_category.parent_id = cte.id
            )
            SELECT * FROM cte
            """
        )
    return Category.objects.filter(is_active=True)
