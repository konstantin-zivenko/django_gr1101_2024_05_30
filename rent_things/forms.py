from django import forms

from .models import Tag, Category


class TagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ["name", "description"]
        widgets = {
            "name": forms.TextInput(
                attrs={"placeholder": "обов'язково введіть назву тегу"}
            ),
            "description": forms.Textarea(
                attrs={"placeholder": "Опишіть як використовувати цей тег"}
            ),
        }


class ThingSearchForm(forms.Form):
    name = forms.CharField(
        max_length=100,
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "Пошук за назвою"}),
    )
    categories = forms.ModelMultipleChoiceField(
        queryset=Category.objects.filter(is_active=True),
        required=False,
        widget=forms.CheckboxSelectMultiple,
    )
    tags = forms.ModelMultipleChoiceField(
        queryset=Tag.objects.filter(is_active=True),
        required=False,
        widget=forms.CheckboxSelectMultiple,
    )
