# django_gr1101_2024_05_30

---

[video 1](https://youtu.be/58N1YS0-Nss) - intro, Django project, django app, structure, settings

[video 2](https://youtu.be/lBUDOGei7PA) - models, migrations, fields

[video 3](https://youtu.be/tQzQwnFHVuA) - models, backwards relationships, admin/superuser, .save(), .objects()

[video 4](https://youtu.be/Qqd4uAgRyV4) - services, lookups, .filter(), .get(), .all()

[video 5](https://youtu.be/K6ah_rLX72I) - views, urls, templates, context

[video 6](https://youtu.be/7vWl307ksYA) - views, urls, templates, generic.ListView, DDT

[video 7](https://youtu.be/gEDYShZBHug) - generic.DetailView, paginations, bootstrap

[video 8](https://youtu.be/kdam-9ofM4I) - forms, intro, forms.Form, forms.ModelForm

[video 9](https://youtu.be/v0k4dZ3tqBI) - forms 2, CreateView, UpdateView, DeleteView, forms.ModelForm, forms.Form

[video 10](https://youtu.be/2UKEBaJHNZo) forms 3, search, recursive request to DB

[video 11](https://youtu.be/Qg22ekDl9us) - sessions, cookies, user authentication, login, logout, change password

[video 12](https://youtu.be/ZqGcC2X4Wuo) - user auth part 2, user registration, password reset

[video 13](https://youtu.be/MFPMuSgI6jQ) - user auth part 3, social auth, GitHub, change username to email

[video 14](https://youtu.be/Qx8u8P-04pM) - rest, intro

[video 15](https://youtu.be/GVxouxNm3BA) - deploy pythonanywhere

